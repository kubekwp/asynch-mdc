package com.abb.rr.asynchmdc;

import java.util.logging.Logger;

public class Worker implements Runnable {
    private static final Logger log = Logger.getLogger(Worker.class.getName());
    private String name;

    public Worker(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        log.info(String.format("Worker [%s] running", name));
    }
}
