package com.abb.rr.asynchmdc;


import java.util.logging.Logger;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

public class MDCInterceptor {

    private static final Logger log = Logger.getLogger(MDCInterceptor.class.getName());

    public MDCInterceptor() {
        log.info(String.format("CONSTRUCT INTERCEPTOR [%s]", Thread.currentThread().getName()));
    }

    @AroundInvoke
    public Object storeLogTrace(InvocationContext ctx) throws Exception {
        log.info(String.format("INTERCEPTOR [%s]", Thread.currentThread().getName()));

        return ctx.proceed();
    }
}
