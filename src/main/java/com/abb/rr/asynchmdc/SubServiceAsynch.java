package com.abb.rr.asynchmdc;

import java.util.Hashtable;
import java.util.logging.Logger;

import javax.ejb.Asynchronous;
import javax.ejb.Stateless;

import org.apache.log4j.MDC;

@Stateless
public class SubServiceAsynch {
    private static final Logger log = Logger.getLogger(SubServiceAsynch.class.getName());
    private Hashtable mdcTable;

//    public SubServiceAsynch() {
//        log.info(String.format("CONSTRUCTOR [%s]", Thread.currentThread().getName()));
//    }

//    @PostConstruct
//    void postConstruct() {
//        log.info(String.format("POST CONSTRUCT [%s]", Thread.currentThread().getName()));
//    }

//    @PostActivate
//    void postActivate() {
//        log.info(String.format("POST ACTIVATE [%s]", Thread.currentThread().getName()));
//    }

    @Asynchronous
//    @Interceptors(MDCInterceptor.class)
    public void performAsynch() {
        MDC.put(TraceFilter.TRACE_ID_KEY, mdcTable.get(TraceFilter.TRACE_ID_KEY));
        log.info(String.format("SubService asynch [%s]", Thread.currentThread().getName()));
    }

    public void setMDCContext(Hashtable mdcTable) {
        this.mdcTable = mdcTable;
    }
}
