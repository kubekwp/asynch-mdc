package com.abb.rr.asynchmdc;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.log4j.MDC;

@Stateless
public class MainService {
    private static final Logger log = Logger.getLogger(MainService.class.getName());

    @EJB
    private SubService subService;

    @EJB
    private SubServiceAsynch subServiceAsynch;

    public MainService() {
    }

    public void performThread() {
        log.info("MainService perform thread started");

        new Thread(new Worker("TH")).start();

        log.info("MainService perform thread finished");
    }

    public void performExecutor() throws InterruptedException {
        log.info("MainService perform executor started");

        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.submit(new Worker("EX"));
        executorService.awaitTermination(5, TimeUnit.SECONDS);

        log.info("MainService perform executor finished");
    }

    public void performEJB() {
        log.info("MainService perform ejb started");

        subService.performSynch();

        log.info("MainService perform ejb finished");
    }

    public void performEJBAsynch() {
        log.info("MainService perform ejb asynch started");

        subServiceAsynch.setMDCContext(MDC.getContext());
        subServiceAsynch.performAsynch();

        log.info("MainService perform ejb asynch finished");
    }
}
