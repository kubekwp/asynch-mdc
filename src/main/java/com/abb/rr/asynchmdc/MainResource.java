package com.abb.rr.asynchmdc;

import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/mainservice")
public class MainResource {
    private static final Logger log = Logger.getLogger(MainResource.class.getName());

    @EJB
    private MainService service;

    @GET
    @Path("th")
    public String performThread() {
        log.info("MainResource invoked");
        service.performThread();
        return "success";
    }

    @GET
    @Path("ex")
    public String performExecutor() throws InterruptedException {
        log.info("MainResource invoked");
        service.performExecutor();
        return "success";
    }

    @GET
    @Path("ejb")
    public String performEJB() {
        log.info("MainResource invoked");
        service.performEJB();
        return "success";
    }

    @GET
    @Path("as")
    public String performEJBAsynch() {
        log.info("MainResource invoked");
        service.performEJBAsynch();
        return "success";
    }
}
