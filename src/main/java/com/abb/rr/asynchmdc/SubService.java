package com.abb.rr.asynchmdc;

import java.util.logging.Logger;

import javax.ejb.Stateless;

@Stateless
public class SubService {
    private static final Logger log = Logger.getLogger(SubService.class.getName());

    public void performSynch() {
        log.info("SubService synchronous");
    }

}
