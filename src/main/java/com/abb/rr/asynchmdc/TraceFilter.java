package com.abb.rr.asynchmdc;

import java.io.IOException;
import java.util.Random;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.log4j.MDC;

public class TraceFilter implements Filter {

    public static final String TRACE_ID_KEY = "traceId";
    private Random random;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        random = new Random();
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {
        try {
            MDC.put(TRACE_ID_KEY, String.format("TRACE [%s]", random.nextInt()));

            filterChain.doFilter(request, response);
        } finally {
            MDC.remove(TRACE_ID_KEY);
        }
    }

    @Override
    public void destroy() {

    }
}
